/* instance_list.rs
 *
 * Copyright 2023 dragnansia
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

use adw::prelude::*;
use adw::subclass::prelude::*;
use gtk::{
    gio,
    glib::{self},
};

use super::instance::GmcInstance;

mod imp {
    use super::*;

    #[derive(Debug, Default, gtk::CompositeTemplate)]
    #[template(resource = "/org/dragnansia/gmc/gtk/instances/instance_list.ui")]
    pub struct GmcInstancesList {
        #[template_child]
        pub groups_list: TemplateChild<gtk::ListBox>,

        #[template_child]
        pub no_groups_instances: TemplateChild<adw::ExpanderRow>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for GmcInstancesList {
        const NAME: &'static str = "GmcInstancesList";
        type Type = super::GmcInstancesList;
        type ParentType = gtk::Box;

        fn class_init(klass: &mut Self::Class) {
            klass.bind_template();
        }

        fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for GmcInstancesList {
        fn constructed(&self) {
            self.parent_constructed();

            let obj = self.obj();
            obj.init();
        }
    }

    impl WidgetImpl for GmcInstancesList {}
    impl BoxImpl for GmcInstancesList {}
}

glib::wrapper! {
    pub struct GmcInstancesList(ObjectSubclass<imp::GmcInstancesList>)
        @extends gtk::Widget, gtk::ScrolledWindow,
        @implements gtk::Accessible, gtk::Buildable, gtk::ConstraintTarget, gio::ActionGroup, gio::ActionMap;
}

impl GmcInstancesList {
    fn init(&self) {
        self.refresh_expansion();
    }

    fn refresh_expansion(&self) {}

    pub fn add_instance(&self, instance: GmcInstance) {
        self.imp().no_groups_instances.add_row(&instance);
    }

    pub fn add_group(&self, group_name: &'static str) {
        let group = adw::ExpanderRow::builder().title(group_name).build();
        self.imp().groups_list.insert(&group, 0);
    }
}
