/* instance_preferences.rs
 *
 * Copyright 2023 dragnansia
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

use adw::prelude::Cast;
use adw::subclass::prelude::*;
use gtk::{gio, glib};

mod imp {
    use super::*;

    #[derive(Debug, Default, gtk::CompositeTemplate)]
    #[template(resource = "/org/dragnansia/gmc/gtk/instances/preferences.ui")]
    pub struct GmcInstancePreferences {}

    #[glib::object_subclass]
    impl ObjectSubclass for GmcInstancePreferences {
        const NAME: &'static str = "GmcInstancePreferences";
        type Type = super::GmcInstancePreferences;
        type ParentType = adw::PreferencesWindow;

        fn class_init(klass: &mut Self::Class) {
            klass.bind_template();
        }

        fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for GmcInstancePreferences {}
    impl WidgetImpl for GmcInstancePreferences {}
    impl WindowImpl for GmcInstancePreferences {}
    impl AdwWindowImpl for GmcInstancePreferences {}
    impl PreferencesWindowImpl for GmcInstancePreferences {}
}

glib::wrapper! {
    pub struct GmcInstancePreferences(ObjectSubclass<imp::GmcInstancePreferences>)
        @extends gtk::Widget, gtk::Window, adw::PreferencesWindow,
        @implements gtk::Accessible, gtk::Buildable, gtk::ConstraintTarget, gio::ActionGroup, gio::ActionMap;
}

impl GmcInstancePreferences {
    pub fn new() -> Self {
        glib::Object::builder().build()
    }
}
