/* instance.rs
 *
 * Copyright 2023 dragnansia
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

use crate::instances::preferences::instance_preference::GmcInstancePreferences;
use adw::glib::clone;
use gtk::glib;
use gtk::prelude::*;
use gtk::subclass::prelude::*;

mod imp {
    use adw::subclass::prelude::{ActionRowImpl, PreferencesRowImpl};
    use gtk::subclass::prelude::*;
    use gtk::Button;

    use super::*;

    #[derive(Debug, Default, gtk::CompositeTemplate)]
    #[template(resource = "/org/dragnansia/gmc/gtk/instances/instance.ui")]
    pub struct GmcInstance {
        #[template_child]
        pub start_instance: TemplateChild<Button>,
        #[template_child]
        pub open_settings: TemplateChild<Button>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for GmcInstance {
        const NAME: &'static str = "GmcInstance";
        type Type = super::GmcInstance;
        type ParentType = adw::ActionRow;

        fn class_init(klass: &mut Self::Class) {
            klass.bind_template();
        }

        fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for GmcInstance {
        fn constructed(&self) {
            self.parent_constructed();

            let obj = self.obj();
            obj.setup_actions();
        }
    }

    impl WidgetImpl for GmcInstance {}
    impl PreferencesRowImpl for GmcInstance {}
    impl ActionRowImpl for GmcInstance {}
    impl ListBoxRowImpl for GmcInstance {}
}

glib::wrapper! {
    pub struct GmcInstance(ObjectSubclass<imp::GmcInstance>)
        @extends gtk::Widget, adw::PreferencesRow, adw::ActionRow,
        @implements gtk::Accessible, gtk::Actionable, gtk::Buildable, gtk::ConstraintTarget;
}

impl GmcInstance {
    pub fn new(name: String, version: String) -> Self {
        glib::Object::builder()
            .property("title", name)
            .property("subtitle", version)
            .build()
    }

    fn setup_actions(&self) {
        let imp = self.imp();

        imp.start_instance
            .connect_clicked(clone!(@weak self as win => move |_| {
                win.start_instance();
            }));

        imp.open_settings
            .connect_clicked(clone!(@weak self as win => move |_| {
                win.open_settings();
            }));
    }

    fn start_instance(&self) {}

    fn open_settings(&self) {
        let settings = GmcInstancePreferences::new();
        settings.present();
    }
}

impl Default for GmcInstance {
    fn default() -> Self {
        glib::Object::builder()
            .property("title", "Instance")
            .property("subtitle", "1.20.2")
            .build()
    }
}
