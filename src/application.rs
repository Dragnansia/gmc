/* application.rs
 *
 * Copyright 2023 dragnansia
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

use adw::subclass::prelude::*;
use gtk::prelude::*;
use gtk::{gio, glib};

use crate::config::VERSION;
use crate::preference::preference::GmcPreferences;
use crate::GmcWindow;

mod imp {
    use super::*;

    #[derive(Debug, Default)]
    pub struct GmcApplication {}

    #[glib::object_subclass]
    impl ObjectSubclass for GmcApplication {
        const NAME: &'static str = "GmcApplication";
        type Type = super::GmcApplication;
        type ParentType = adw::Application;
    }

    impl ObjectImpl for GmcApplication {
        fn constructed(&self) {
            self.parent_constructed();
            let obj = self.obj();
            obj.setup_actions();
            obj.set_accels_for_action("app.quit", &["<primary>q"]);
            obj.set_accels_for_action("win.open_instance_folder", &["<primary>i"]);
            obj.set_accels_for_action("app.preferences", &["<primary>p"]);
            obj.set_accels_for_action("win.add_instance", &["<primary>d"]);
        }
    }

    impl ApplicationImpl for GmcApplication {
        // We connect to the activate callback to create a window when the application
        // has been launched. Additionally, this callback notifies us when the user
        // tries to launch a "second instance" of the application. When they try
        // to do that, we'll just present any existing window.
        fn activate(&self) {
            let application = self.obj();
            // Get the current window or create one if necessary
            let window = if let Some(window) = application.active_window() {
                window
            } else {
                let window = GmcWindow::new(&*application);
                window.upcast()
            };

            // Ask the window manager/compositor to present the window
            window.present();
        }
    }

    impl GtkApplicationImpl for GmcApplication {}
    impl AdwApplicationImpl for GmcApplication {}
}

glib::wrapper! {
    pub struct GmcApplication(ObjectSubclass<imp::GmcApplication>)
        @extends gio::Application, gtk::Application, adw::Application,
        @implements gio::ActionGroup, gio::ActionMap;
}

impl GmcApplication {
    pub fn new(application_id: &str, flags: &gio::ApplicationFlags) -> Self {
        glib::Object::builder()
            .property("application-id", application_id)
            .property("flags", flags)
            .build()
    }

    fn setup_actions(&self) {
        let quit_action = gio::ActionEntry::builder("quit")
            .activate(move |app: &Self, _, _| app.quit())
            .build();
        let about_action = gio::ActionEntry::builder("about")
            .activate(move |app: &Self, _, _| app.show_about())
            .build();
        let preference_action = gio::ActionEntry::builder("preferences")
            .activate(move |app: &Self, _, _| app.show_preference())
            .build();
        self.add_action_entries([quit_action, about_action, preference_action]);
    }

    fn show_preference(&self) {
        let window = self.active_window().unwrap();

        let preferences = GmcPreferences::new(&window);
        preferences.present();
    }

    fn show_about(&self) {
        let window = self.active_window().unwrap();
        let about = adw::AboutWindow::builder()
            .transient_for(&window)
            .application_name("gmc")
            .application_icon("org.dragnansia.gmc")
            .developer_name("Dragnansia")
            .version(VERSION)
            .developers(vec!["Dragnansia"])
            .copyright("© 2023 Dragnansia")
            .build();

        about.present();
    }
}
