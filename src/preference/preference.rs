/* preference.rs
 *
 * Copyright 2023 dragnansia
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

use adw::prelude::Cast;
use adw::subclass::prelude::*;
use gtk::{gio, glib};

mod imp {
    use super::*;

    #[derive(Debug, Default, gtk::CompositeTemplate)]
    #[template(resource = "/org/dragnansia/gmc/gtk/preference.ui")]
    pub struct GmcPreferences {}

    #[glib::object_subclass]
    impl ObjectSubclass for GmcPreferences {
        const NAME: &'static str = "GmcPreferences";
        type Type = super::GmcPreferences;
        type ParentType = adw::PreferencesWindow;

        fn class_init(klass: &mut Self::Class) {
            klass.bind_template();
        }

        fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for GmcPreferences {}
    impl WidgetImpl for GmcPreferences {}
    impl WindowImpl for GmcPreferences {}
    impl AdwWindowImpl for GmcPreferences {}
    impl PreferencesWindowImpl for GmcPreferences {}
}

glib::wrapper! {
    pub struct GmcPreferences(ObjectSubclass<imp::GmcPreferences>)
        @extends gtk::Widget, gtk::Window, adw::PreferencesWindow,
        @implements gtk::Accessible, gtk::Buildable, gtk::ConstraintTarget, gio::ActionGroup, gio::ActionMap;
}

impl GmcPreferences {
    pub fn new<P: glib::IsA<gtk::Window>>(window: &P) -> GmcPreferences {
        glib::Object::builder()
            .property("transient-for", window.clone().upcast())
            .build()
    }
}
