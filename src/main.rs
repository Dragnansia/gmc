/* main.rs
 *
 * Copyright 2023 dragnansia
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

mod account;
mod application;
mod config;
mod instances;
mod preference;
mod window;

use std::fs;

use self::application::GmcApplication;
use self::window::GmcWindow;

use config::{GETTEXT_PACKAGE, LOCALEDIR, PKGDATADIR};
use ftlog::appender::{Duration, FileAppender, Period};
use gettextrs::{bind_textdomain_codeset, bindtextdomain, textdomain};
use gtk::prelude::*;
use gtk::{gio, glib};

fn main() -> glib::ExitCode {
    init_logger();

    // Set up gettext translations
    bindtextdomain(GETTEXT_PACKAGE, LOCALEDIR).expect("Unable to bind the text domain");
    bind_textdomain_codeset(GETTEXT_PACKAGE, "UTF-8")
        .expect("Unable to set the text domain encoding");
    textdomain(GETTEXT_PACKAGE).expect("Unable to switch to the text domain");

    // Load resources
    let resources = gio::Resource::load(PKGDATADIR.to_owned() + "/gmc.gresource")
        .expect("Could not load resources");
    gio::resources_register(&resources);

    let app = GmcApplication::new("org.dragnansia.gmc", &gio::ApplicationFlags::empty());
    app.run()
}

fn init_logger() {
    let Some(data_path) = dirs::data_dir() else {
        println!("Can't find data path to configure logger");
        return;
    };

    let mut log_path = data_path.join("logs");
    if !log_path.exists() {
        let _ = fs::create_dir_all(&log_path);
    }

    log_path = log_path.join("current.log");
    let writer = FileAppender::rotate_with_expire(log_path, Period::Day, Duration::weeks(1));

    ftlog::Builder::new()
        .max_log_level(log::LevelFilter::Trace)
        .root(writer)
        .utc()
        .try_init()
        .expect("logger build or set failed");
}
