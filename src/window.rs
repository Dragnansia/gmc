/* window.rs
 *
 * Copyright 2023 dragnansia
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

use std::fs;

use crate::instances::instance::GmcInstance;
use adw::subclass::prelude::*;
use gtk::prelude::*;
use gtk::{gio, glib};

mod imp {
    use crate::instances::{instance::GmcInstance, instance_list::GmcInstancesList};

    use super::*;

    #[derive(Debug, Default, gtk::CompositeTemplate)]
    #[template(resource = "/org/dragnansia/gmc/window.ui")]
    pub struct GmcWindow {
        // Template widgets
        #[template_child]
        pub header_bar: TemplateChild<adw::HeaderBar>,
        #[template_child]
        pub instances: TemplateChild<GmcInstancesList>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for GmcWindow {
        const NAME: &'static str = "GmcWindow";
        type Type = super::GmcWindow;
        type ParentType = adw::ApplicationWindow;

        fn class_init(klass: &mut Self::Class) {
            GmcInstancesList::ensure_type();
            GmcInstance::ensure_type();

            klass.bind_template();
        }

        fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for GmcWindow {
        fn constructed(&self) {
            self.parent_constructed();
            let obj = self.obj();
            obj.setup_actions();
        }
    }
    impl WidgetImpl for GmcWindow {}
    impl WindowImpl for GmcWindow {}
    impl ApplicationWindowImpl for GmcWindow {}
    impl AdwApplicationWindowImpl for GmcWindow {}
}

glib::wrapper! {
    pub struct GmcWindow(ObjectSubclass<imp::GmcWindow>)
        @extends gtk::Widget, gtk::Window, gtk::ApplicationWindow, adw::ApplicationWindow,
        @implements gio::ActionGroup, gio::ActionMap;
}

impl GmcWindow {
    pub fn new<P: IsA<gtk::Application>>(application: &P) -> GmcWindow {
        glib::Object::builder()
            .property("application", application)
            .build()
    }
}

impl GmcWindow {
    fn setup_actions(&self) {
        let open_instance_action = gio::ActionEntry::builder("open_instance_folder")
            .activate(move |app: &Self, _, _| app.open_instance_folder())
            .build();

        let add_instance = gio::ActionEntry::builder("add_instance")
            .activate(move |app: &Self, _, _| app.add_default_instance())
            .build();

        self.add_action_entries([open_instance_action, add_instance]);
    }

    fn add_default_instance(&self) {
        self.imp()
            .instances
            .add_instance(GmcInstance::new("Test".into(), "1.20.2".into()));
    }

    fn open_instance_folder(&self) {
        let Some(share_path) = dirs::data_dir() else {
            log::error!("Can't find data directory");
            return;
        };

        let instance_path = share_path.join("instances");
        if !instance_path.exists() {
            let _ = fs::create_dir_all(&instance_path);
        }

        let _ = open::that(instance_path);
    }
}
